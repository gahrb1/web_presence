import os.path

production = int(os.getenv("PRODUCTION"))
debug = False
if not production:
    debug = int(os.getenv("DEBUG"))
proc_name = "landing_page_gunicorn"
logfile = "/var/www/gahrb.dev/logs/gunicorn.log"
accessfile = "/var/www/gahrb.dev/logs/access.log"

os.makedirs(os.path.dirname(logfile), exist_ok=True)
os.makedirs(os.path.dirname(logfile), exist_ok=True)

loglevel = "info"
errorlog = logfile
accesslog = accessfile

bind = "127.0.0.1:8000"
backlog = 2048
if production:
    bind = os.getenv("GUNICORN_BIND")
    backlog = int(os.getenv("GUNICORN_BACKLOG"))

if debug:
    loglevel = "debug"
    errorlog = "-"
    accesslog = "-"


def post_fork(server, worker):
    server.log.info("Worker spawned (pid: %s)", worker.pid)


def pre_fork(server, worker):
    pass


def pre_exec(server):
    server.log.info("Forked child, re-executing.")


def when_ready(server):
    server.log.info("Server is ready. Spwawning workers")
