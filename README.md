# web_presence

[![Latest Release](https://gitlab.com/gahrb1/web_presence/-/badges/release.svg)](https://gitlab.com/gahrb1/web_presence/-/releases)
[![coverage report](https://gitlab.com/gahrb1/web_presence/badges/main/coverage.svg)](https://gitlab.com/gahrb1/web_presence/-/commits/main)
[![pipeline status](https://gitlab.com/gahrb1/web_presence/badges/main/pipeline.svg)](https://gitlab.com/gahrb1/web_presence/-/commits/main)

## Content

Provides the default web presence for [gahrb.dev](https://gahrb.dev)
