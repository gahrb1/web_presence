from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path, include
from django.views.generic import RedirectView

from catalog import views

urlpatterns = [
    path("admin/", admin.site.urls, name="admin"),
    ###
    # Admin related urls
    ###
    path(
        "admin/password_reset/",
        auth_views.PasswordResetView.as_view(),
        name="admin_password_reset",
    ),
    path(
        "admin/password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
]

urlpatterns += [
    path("home/", include(("catalog.urls", "catalog"), namespace="catalog")),
    # All Auth urls
    path("accounts/", include("allauth.urls")),
]

# The default root url should point to catalog
urlpatterns += [
    path("", RedirectView.as_view(url="home/", permanent=False)),
    path("io/<str:unique_url>", views.get_io_forward, name="io_forward"),
    path("favicon.ico", RedirectView.as_view(url=staticfiles_storage.url("favicon.ico")),),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "catalog.views.handler404"
# handler500 = 'catalog.views.handler404'
