"""
WSGI config for landing_page project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application
from whitenoise import WhiteNoise

from landing_page import settings

os.environ["DJANGO_SETTINGS_MODULE"] = "landing_page.settings"
application = WhiteNoise(get_wsgi_application(), root=settings.STATIC_ROOT)
# application.add_files("/path/to/more/static/files", prefix="more-files/")
