#!/bin/bash

cd /opt/app/
/opt/venv/bin/python manage.py migrate --noinput
/opt/venv/bin/python manage.py createsuperuser --username $SUPERUSER --noinput || true