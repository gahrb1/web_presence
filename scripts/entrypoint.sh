#!/bin/bash
APP_PORT=${PORT:-8000}
source /opt/app/scripts/migrate.sh
source /opt/app/scripts/collectstatic.sh &&
/opt/venv/bin/gunicorn --worker-tmp-dir /dev/shm landing_page.wsgi:application --bind "0.0.0.0:${APP_PORT}"