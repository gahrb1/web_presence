FROM python:3.10-slim

# Curl is required to download the cockroach cert. Vim is just the best ;-)
RUN apt-get update && apt-get install vim curl -y

# set work directory
WORKDIR /opt/app

# copy project
COPY . /opt/app

# ENV Variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# Set all user permissions
RUN python3.10 -m venv /opt/venv
RUN mkdir -p /var/www/

# get cockroachlabs certificate
RUN curl -s --create-dirs -o $HOME/.postgresql/root.crt -O https://cockroachlabs.cloud/clusters/$CLUSTER_ID/cert
RUN /opt/venv/bin/pip install pip --upgrade
RUN /opt/venv/bin/pip install -r requirements/requirements.txt

RUN echo "Build Success! Ready to start web presence..."
CMD ["/bin/bash", "/opt/app/scripts/entrypoint.sh"]