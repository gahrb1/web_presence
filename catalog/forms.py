from django import forms


class UniqueUrlForm(forms.Form):
    template_name = "catalog/form_snippet.html"
    linked_to = forms.CharField(max_length=64, required=True)
    greeting = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "name": "body",
                "rows": "3",
                "cols": "25",
            }
        )
    )
