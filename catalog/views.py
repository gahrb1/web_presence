from __future__ import annotations

import tempfile
from pathlib import Path

from django.contrib import messages
from django.http import HttpRequest, HttpResponseRedirect
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.vary import vary_on_cookie
from django.views.generic import View, TemplateView

from catalog.forms import UniqueUrlForm

# Self created Models
from catalog.models import Project, TechElement, EntranceURL
from catalog.templatetags.media import (
    generate_unique_url,
    get_db_user_view_level,
    get_session_data,
    get_user_view_level,
)


class PingView(View):
    def get(self, request):
        return HttpResponse("python/django", status=200)


@method_decorator(csrf_exempt, name="dispatch")
class ProjectsView(TemplateView):
    template_name = "catalog/projects.html"

    def get_context_data(self):
        user_view_level = get_user_view_level(request=self.request)
        projects = Project.objects.filter(view_level__lte=user_view_level + 1).order_by(
            "-created"
        )
        context = {
            "projects": projects,
            "user_view_level": user_view_level,
        }
        return context


@method_decorator(csrf_exempt, name="dispatch")
class ProjectView(generic.DetailView):
    template_name = "catalog/project.html"

    def get(self, request, id=None, *args, **kwargs):
        user_view_level = get_user_view_level(request=self.request)
        project = Project.objects.get(id=id)
        if project.view_level > user_view_level:
            messages.error(
                self.request,
                f"Sorry, you do not have the required view-level to see this project",
            )
            return HttpResponseRedirect(reverse("catalog:projects"))
        context = {
            "project": project,
            "techstack": project.techstack.all(),
            "user_view_level": user_view_level,
        }
        return render(request=request, template_name="catalog/project.html", context=context)


@method_decorator(csrf_exempt, name="dispatch")
class TechStackView(TemplateView):
    template_name = "catalog/techstack.html"

    def get_context_data(self, *args, **kwargs):
        user_view_level = get_user_view_level(request=self.request)
        techstack = TechElement.objects.exclude(
            id__in=TechElement.objects.filter(svg__lt=1, icon_src__lt=1).order_by(
                "name"
            ),
        )
        projects = Project.objects.filter(view_level__lte=user_view_level + 1).order_by(
            "view_level", "-created"
        )
        context = {
            "techstack": techstack,
            "projects": projects,
        }
        return context


@method_decorator(csrf_exempt, name="dispatch")
class TechElementView(generic.ListView):
    def get(self, request, tech_id=None, *args, **kwargs):
        assert tech_id is not None
        products = list(TechElement.objects.filter(id=tech_id).values())
        if len(products) == 0:
            return render(request, "main/home.html")
        return JsonResponse(products, safe=False)


# Create your views here.
tempfile = Path(tempfile.gettempdir(), "Count.txt")


@vary_on_cookie
def index(request: HttpRequest, *args, **kwargs):
    count = read_count()
    write_count(count + 1)
    context = {
        "overview": True,
        "view_count": count,
        "num_projects": Project.objects.all().count(),
        "num_techstack": TechElement.objects.all().count(),
    }
    if request.GET.get("not_implemented"):
        messages.add_message(request, 15, "This site is currently under construction.")
    return render(request=request, template_name="main/home.html", context=context)


@vary_on_cookie
def login(request: HttpRequest):
    return render(request, "main/login.html")


def read_count() -> int:
    if tempfile.exists():
        with open(tempfile) as fp:
            content = fp.readline()
        return int(content)
    return 0


def write_count(count: int) -> None:
    with open(tempfile, "w+") as fp:
        fp.write(str(count))


def handler404(request, *args, **kwargs) -> HttpResponse:
    messages.error(request, "Unknown page.")
    return HttpResponseRedirect("/")


def get_unique_url(request: HttpRequest | None) -> HttpResponse:
    """
    This function creates a new EntranceURL, and returns its url with the prefix "https://gahrb.dev/io/".
    The url is written into the db together with the request params with additional information where this url
    was published. The param must be specified with the key "linked_to". If the param is not specified, the timestamp
    is used as a fallback.
    :param request: HttpRequest
    :return: str
    """
    if request is not None and request.user.is_staff:
        # if this is a POST request we need to process the form data
        if request.method == "POST":
            # create a form instance and populate it with data from the request:
            form = UniqueUrlForm(request.POST)
            # check whether it's valid:
            if form.is_valid():
                form_data = form.cleaned_data
                new_url = generate_unique_url(request.get_host())
                entrance_element = EntranceURL.objects.create(
                    url=new_url,
                    linked_to=form_data["linked_to"],
                    greeting=form_data["greeting"],
                )
                context = {
                    "id": entrance_element.id,
                    "url": entrance_element.url,
                    "linked_to": entrance_element.linked_to,
                    "greeting": entrance_element.greeting,
                    "created": entrance_element.date_of_creation,
                }
                return render(
                    request=request,
                    template_name="catalog/unique_url_post.html",
                    context=context,
                )
        form = UniqueUrlForm()
        return render(
            request=request,
            template_name="catalog/unique_url_get.html",
            context={"form": form},
        )
    # Default: return 404
    return handler404(request)


@vary_on_cookie
def get_io_forward(request: HttpRequest, unique_url: str) -> HttpResponse:
    entrance_urls = EntranceURL.objects.all()
    for entrance_url in entrance_urls:
        if unique_url == entrance_url.url.split("/")[-1]:
            request.session["io"] = str(entrance_url.id)
            # Don't overwrite a view_level if it already exists
            request.session["view_level"] = max(2, get_db_user_view_level(request))
            messages.add_message(request, 10, entrance_url.greeting)
            entrance_url.accessed_by.add(request.session.session_key)
            break
    return HttpResponseRedirect("/")


@vary_on_cookie
def get_your_data(request: HttpRequest, user_id: int = None) -> HttpResponse:
    context = {}
    if (
        request.user.is_authenticated
        and request.user.id == user_id
        or request.user.is_staff
    ):
        session_data = get_session_data(
            request=request
        )
        context.update(
            {
                "collected_data": session_data,
            }
        )
    return render(request, "catalog/your_data.html", context=context)
