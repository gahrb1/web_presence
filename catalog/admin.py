from django.contrib import admin
from django.contrib.sessions.models import Session

# Register your models here.
from django.utils.safestring import mark_safe

from catalog.models import AppAuthor, Media, Project, TechElement, EntranceURL, PageView


@admin.register(AppAuthor)
class AppAuthorAdmin(admin.ModelAdmin):
    list_display = ["name", "email"]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["name", "active", "link", "created", "view_level", "status"]
    list_filter = ["created", "active", "status", "view_level", "techstack"]


@admin.register(TechElement)
class TechElementAdmin(admin.ModelAdmin):
    list_display = ["name", "icon_src", "svg", "thumbnail"]
    list_filter = ("project",)

    @mark_safe
    def thumbnail(self, obj) -> str:
        if obj.svg is not None and len(obj.svg):
            return f'<svg id="{obj.name}">{obj.svg}</svg>'
        elif obj.icon_src is not None and len(obj.icon_src):
            return f'<img src="{obj.icon_src}" height="20" width="80"/>'
        return "No Preview"

    thumbnail.short_description = "thumbnail"
    readonly_fields = ("thumbnail",)


@admin.register(EntranceURL)
class EntranceURLAdmin(admin.ModelAdmin):
    list_display = ["id", "url", "linked_to", "date_of_creation"]
    list_filter = ["accessed_by"]


@admin.register(Session)
class SessionsAdmin(admin.ModelAdmin):
    list_display = ["session_key", "expire_date"]
    list_filter = ["expire_date"]


@admin.register(PageView)
class PageViewAdmin(admin.ModelAdmin):
    list_display = ["id", "url", "accessed", "user", "session"]
    list_filter = ["user", "url", "accessed", "session"]


@admin.register(Media)
class MediaAdmin(admin.ModelAdmin):
    list_display = ["id", "url", "image"]
    list_filter = ["id", "url", "image"]
