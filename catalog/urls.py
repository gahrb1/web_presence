from django.urls import path

from catalog import views

urlpatterns = [
    path("", views.index, name="index"),
    path("index.html", views.index, name="home"),
    path("ping.html", views.index, name="ping"),
    path("login.html", views.login, name="login"),
    path("about.html", views.index, name="about"),
    path("stats/", views.PingView.as_view(), name="stats"),
    path("ping/", views.PingView.as_view(), name="ping"),
    path("projects/", views.ProjectsView.as_view(), name="projects"),
    path("project/<uuid:id>/", views.ProjectView.as_view(), name="project"),
    path("techstack/", views.TechStackView.as_view(), name="techstack"),
    path("techelement/<uuid:id>/", views.TechElementView.as_view(), name="techelem"),
    path("unique_url/", views.get_unique_url, name="unique_url"),
    path("your_data/", views.get_your_data, name="session_data"),
    path("your_data/<int:user_id>", views.get_your_data, name="session_data"),
]
