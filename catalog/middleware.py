from django.contrib.sessions.models import Session

from catalog.models import PageView


class ViewTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request, *args, **kwargs):
        if "admin" not in request.path:
            if not request.session.exists(request.session.session_key):
                request.session.create()

            user = None
            if request.user.is_authenticated:
                user = request.user
            PageView.objects.create(
                url=request.path,
                user_agent=request.META.get("HTTP_USER_AGENT"),
                ip=request.META.get("REMOTE_ADDR"),
                referer=request.META.get("HTTP_REFERER"),
                session=Session.objects.get(session_key=request.session.session_key),
                user=user,
            )
        response = self.get_response(request, *args, **kwargs)
        return response
