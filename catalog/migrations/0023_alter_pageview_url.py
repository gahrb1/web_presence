# Generated by Django 4.1 on 2022-09-07 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("catalog", "0022_remove_pageview_session_instance"),
    ]

    operations = [
        migrations.AlterField(
            model_name="pageview",
            name="url",
            field=models.TextField(),
        ),
    ]
