# Generated by Django 4.1 on 2022-09-02 14:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("catalog", "0010_alter_entranceurl_linked_to_usersession"),
    ]

    operations = [
        migrations.AddField(
            model_name="usersession",
            name="entrance_url",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="catalog.entranceurl",
            ),
        ),
        migrations.AlterField(
            model_name="entranceurl",
            name="linked_to",
            field=models.CharField(default="2022-09-02 14:39:06", max_length=250),
        ),
    ]
