# Generated by Django 4.1 on 2022-09-07 12:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    replaces = [
        ("catalog", "0019_pageview"),
        ("catalog", "0020_pageview_session_key_pageview_user"),
        ("catalog", "0021_rename_session_key_pageview_session_instance"),
        ("catalog", "0022_remove_pageview_session_instance"),
        ("catalog", "0023_alter_pageview_url"),
        ("catalog", "0024_pageview_session"),
        ("catalog", "0025_alter_pageview_session"),
        ("catalog", "0026_alter_pageview_session"),
        ("catalog", "0027_remove_pageview_session_pageview_session_key"),
        ("catalog", "0028_alter_pageview_user"),
        ("catalog", "0029_alter_pageview_referer"),
        ("catalog", "0030_entranceurl_accessed_by"),
        ("catalog", "0031_remove_pageview_session_key_pageview_session"),
    ]

    dependencies = [
        ("sessions", "0001_initial"),
        (
            "catalog",
            "0015_mysession_mysocialaccount_customuser_squashed_0018_delete_mysocialaccount",
        ),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name="entranceurl",
            name="accessed_by",
            field=models.ManyToManyField(blank=True, to="sessions.session"),
        ),
        migrations.CreateModel(
            name="PageView",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("url", models.TextField()),
                ("accessed", models.DateTimeField(auto_now_add=True)),
                ("user_agent", models.TextField(blank=True, default="")),
                ("ip", models.GenericIPAddressField()),
                ("referer", models.TextField(blank=True, default="", null=True)),
                (
                    "user",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "session",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="sessions.session",
                    ),
                ),
            ],
        ),
    ]
