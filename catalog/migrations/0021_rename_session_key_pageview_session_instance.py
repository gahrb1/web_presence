# Generated by Django 4.1 on 2022-09-07 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("catalog", "0020_pageview_session_key_pageview_user"),
    ]

    operations = [
        migrations.RenameField(
            model_name="pageview",
            old_name="session_key",
            new_name="session_instance",
        ),
    ]
