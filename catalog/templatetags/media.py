import random
import re
import string
from pathlib import Path

from django import template
from django.conf import settings
from django.contrib.sessions.models import Session
from django.contrib.staticfiles.storage import StaticFilesStorage
from django.http import HttpRequest

from catalog.models import Project

register = template.Library()


@register.simple_tag
def get_sample_img() -> str:
    # Return a random image from the project images with the file name "dalle_gahrb_<id>.png"
    storage = StaticFilesStorage(location=Path(settings.STATIC_ROOT, "img"))
    files = Path(storage.location).glob("dalle_gahrb_*.png")
    pattern = "dalle_gahrb_(\d+).png"
    ids = [f for f in files if re.search(pattern, str(f))]
    value = random.randint(0, len(ids) - 1)
    file = f"dalle_gahrb_{value}.png"
    return storage.url("img/" + file)


@register.filter(name="get_dalle_img")
def get_dalle_img(value: int) -> str:
    return f"img/dalle_gahrb_{value}.png"


@register.simple_tag
def get_project_view_level(project: Project = None) -> int:
    if project is not None:
        return project.view_level
    return 0


@register.filter(name="get_ts_len")
def get_ts_len(project: Project = None) -> int:
    if project is not None:
        return project.techstack.count()
    return 0


@register.filter(name="get_blur")
def get_blur(project: Project = None, user_view_level: int = 0) -> bool:
    if get_project_view_level(project=project) < user_view_level + 1:
        return False
    return True


@register.simple_tag
def generate_unique_url(base_url: str = "https://gahrb.dev", length: int = 3) -> str:
    return (
            base_url
            + "/io/"
            + "".join(
        random.SystemRandom().choice(string.ascii_letters + string.digits)
        for _ in range(length)
    )
    )


@register.simple_tag
def get_session_data(request: HttpRequest) -> dict:
    all_sessions = Session.objects.all()
    if request.session.session_key in all_sessions:
        return Session.objects.get(pk=request.session.session_key).get_decoded()
    # No Session created (or written to the db) yet
    return {}


@register.simple_tag
def get_user_view_level(request: HttpRequest) -> int:
    user_view_level = 0
    if "view_level" in request.session:
        user_view_level = request.session["view_level"]
    user_view_level = max(user_view_level, get_db_user_view_level(request=request))
    return user_view_level


@register.simple_tag
def get_db_user_view_level(request: HttpRequest) -> int:
    if request.user.is_staff:
        return 4
    if request.user.is_authenticated:
        return 3
    return 0
