from __future__ import annotations

from importlib.resources import _

from django.conf import settings
from django.contrib.sessions.models import Session
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.safestring import mark_safe
import uuid


class AppAuthor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=250)
    email = models.EmailField()

    def __str__(self):
        return f"{self.name} -- {str(self.id)[:5]}"


class Project(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=250)
    description = models.TextField(max_length=2**14)
    active = models.BooleanField()
    link = models.URLField()
    created = models.DateField(auto_now_add=False, blank=False)
    view_level = models.IntegerField(default=1)
    author = models.ForeignKey("AppAuthor", on_delete=models.SET_NULL, null=True)
    techstack = models.ManyToManyField("TechElement")
    media = models.ManyToManyField("Media")

    DEV_STATUS = (
        ("m", "Maintained"),
        ("f", "Finalized"),
        ("d", "Deprecated"),
        ("p", "Planned"),
    )

    status = models.CharField(
        max_length=1,
        choices=DEV_STATUS,
        blank=True,
        default="p",
        help_text="Project status",
    )

    def __str__(self):
        return f"{self.name} -- {str(self.id)[:5]}"

    def escape_name(self):
        return str(self.name).replace(" ", "_")

    class Meta:
        ordering = ["-created"]


class TechElement(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=250)
    icon_src = models.URLField(blank=True)
    svg = models.TextField(blank=True)

    def __str__(self) -> str:
        return f"{self.name} -- {str(self.id)[:5]}"

    @mark_safe
    def icon_to_html(self) -> str | None:
        if self.svg is not None and len(self.svg):
            return f"""
            <div class="img-full">
            <svg id=\"{self.name}-svg\" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 51" preserveAspectRatio="none">
             {self.svg}
            </svg>
            </div>
            """
        elif self.icon_src is not None and len(self.icon_src):
            return f'<img src="{self.icon_src}" class="img-full" alt="" width="" height="" />'


class EntranceURL(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.URLField(blank=False, unique=True)
    linked_to = models.CharField(
        max_length=250,
        blank=False,
    )
    date_of_creation = models.DateTimeField(auto_now_add=True)
    greeting = models.TextField(blank=True, default="")
    accessed_by = models.ManyToManyField(
        Session,
        blank=True,
    )

    def __str__(self) -> str:
        return f"{self.url} -- {str(self.id)[:5]}"


class PageView(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.TextField(blank=False, unique=False)
    accessed = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    user_agent = models.TextField(blank=True, null=True, default="")
    ip = models.GenericIPAddressField(blank=False)
    referer = models.TextField(blank=True, default="", null=True)
    session = models.ForeignKey(
        Session,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self) -> str:
        return f"{self.page} -- {str(self.id)[:5]}"


class Media(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.TextField(blank=True, unique=False)
    image = models.ImageField(upload_to="media/", blank=True, null=True)

    def clean(self):
        if not(self.url or self.image):
            raise ValidationError(_("Media must have either url or image."))

    def __str__(self) -> str:
        return str(self.id)

    def get_media_url(self) -> str:
        if self.image:
            return self.image.url
        else:
            # TODO: for dalle query it from # GET https://api.openai.com/v1/files/{file_id}
            return self.url
