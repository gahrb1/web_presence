
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.test import TestCase
from django.utils import timezone

from catalog.models import PageView
import datetime as dt


class PageViewTest(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(
            username="test_user",
            email="",
            password="test_password",
        )
        PageView.objects.create(
            url="test_url",
            user_agent="test_user_agent",
            ip="192.168.0.1",
            referer="test_referer",
            session=Session.objects.create(
                session_key="test_session_key",
                session_data="test_session_data",
                expire_date=timezone.localtime(timezone.now()),
            ),
            user=test_user,
        )

    def test_exist(self):
        page_view = PageView.objects.get(url="test_url")
        self.assertEqual(page_view.url, "test_url")
        self.assertEqual(page_view.user_agent, "test_user_agent")
        self.assertEqual(page_view.ip, "192.168.0.1")
        self.assertEqual(page_view.referer, "test_referer")
        self.assertEqual(
            page_view.session, Session.objects.get(session_key="test_session_key")
        )
        self.assertEqual(page_view.user, User.objects.get(username="test_user"))
        self.assertIsInstance(page_view.accessed, dt.datetime)
