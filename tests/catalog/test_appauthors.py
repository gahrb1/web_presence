from django.test import TestCase
from catalog.models import AppAuthor


class AppAuthorTestCase(TestCase):
    def setUp(self):
        AppAuthor.objects.create(name="test_user", email="test@dev.dev")

    def test___str__(self):
        self.assertEqual(
            "test_user -- " + str(AppAuthor.objects.get(name="test_user").id)[0:5],
            AppAuthor.objects.get(name="test_user").__str__(),
        )

    def test_app_author(self):
        default_app_author = AppAuthor.objects.get(name="test_user")
        self.assertEqual(default_app_author.email, "test@dev.dev")
