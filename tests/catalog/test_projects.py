import random

from django.test import TestCase
from django.utils import timezone

from catalog.models import AppAuthor, Project, TechElement
from catalog.templatetags.media import get_project_view_level, get_blur, get_ts_len


class ProjectTestCase(TestCase):
    def setUp(self):
        AppAuthor.objects.create(name="test_user", email="test@dev.dev")
        TechElement.objects.create(name="test_tech_element_1", icon_src="+")
        TechElement.objects.create(name="test_tech_element_2", svg="+")
        TechElement.objects.create(name="test_tech_element_")
        Project.objects.create(
            name="test_project", active=False, created=timezone.now()
        )
        Project.objects.create(
            name="test project 2", active=False, created=timezone.now()
        )

    def test___str__(self):
        self.assertEqual(
            "test_project -- " + str(Project.objects.get(name="test_project").id)[0:5],
            Project.objects.get(name="test_project").__str__(),
        )

    def test_elem_proj_relation(self):
        default_proj = Project.objects.get(name="test_project")
        default_proj.techstack.set(
            [
                TechElement.objects.get(name="test_tech_element_1"),
                TechElement.objects.get(name="test_tech_element_2"),
            ]
        )
        self.assertEqual(default_proj.techstack.count(), 2)

    def test_elem_count(self):
        self.assertEqual(TechElement.objects.count(), 3)

    def test_icon_to_html_img(self):
        self.assertIn(
            "<img src=",
            TechElement.objects.get(name="test_tech_element_1").icon_to_html(),
        )

    def test_icon_to_html_svg(self):
        self.assertIn(
            "<svg id=",
            TechElement.objects.get(name="test_tech_element_2").icon_to_html(),
        )

    def test_escape_name(self):
        self.assertEqual(
            "test_project_2",
            Project.objects.get(name="test project 2").escape_name(),
        )

    def test_escape_name_no_whitecharacter(self):
        for project in Project.objects.all():
            self.assertNotIn(" ", project.escape_name())

    def test_get_view_level(self):
        for project in Project.objects.all():
            self.assertIn(get_project_view_level(project), range(4))

    def test_get_ts_len(self):
        for project in Project.objects.all():
            self.assertIn(get_ts_len(project), range(4))

    def test_get_blur(self):
        user_view_level = random.randint(0, 5)
        for project in Project.objects.all():
            self.assertIsInstance(
                get_blur(project=project, user_view_level=user_view_level), bool
            )
