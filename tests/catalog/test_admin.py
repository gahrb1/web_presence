# TestClasses and TestFunctions for catalog/admin.py:

import pytest
from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone

from catalog.admin import (
    TechElementAdmin,
    EntranceURLAdmin,
    SessionsAdmin,
    PageViewAdmin,
)
from catalog.models import TechElement, EntranceURL, PageView
from catalog.templatetags.media import generate_unique_url


class MockRequest:
    pass


class MockSuperUser:
    def has_perm(self, perm, obj=None):  # nqa
        return True

    class MockUser:
        def has_module_perms(self, app_label):
            return app_label == "modeladmin"

    class MockViewUser(MockUser):
        def has_perm(self, perm, obj=None):
            return perm == "modeladmin.view_band"

    class MockAddUser(MockUser):
        def has_perm(self, perm, obj=None):
            return perm == "modeladmin.add_band"

    class MockChangeUser(MockUser):
        def has_perm(self, perm, obj=None):
            return perm == "modeladmin.change_band"

    class MockDeleteUser(MockUser):
        def has_perm(self, perm, obj=None):
            return perm == "modeladmin.delete_band"


pytestmark = pytest.mark.parametrize(
    "user,expected",
    [
        ([MockSuperUser(), True]),
        (MockSuperUser.MockUser(), False),
        (MockSuperUser.MockViewUser(), True),
        (MockSuperUser.MockAddUser(), False),
        (MockSuperUser.MockChangeUser(), False),
        (MockSuperUser.MockDeleteUser(), False),
    ],
)


class TestTechElementAdmin(TestCase):
    def setUp(self):
        self.test_tech_element_1 = TechElement.objects.create(
            name="test_tech_element_src", icon_src="+"
        )
        self.test_tech_element_2 = TechElement.objects.create(
            name="test_tech_element_svg", svg="+"
        )
        self.test_tech_element_3 = TechElement.objects.create(  # no icon_src or svg
            name="test_tech_element_no_src_svg"
        )
        self.techelement_admin = TechElementAdmin(TechElement, None)

    @pytest.mark.django_db(False)
    def test_thumbnail_img(self):
        self.assertIn(
            "<img src=",
            self.techelement_admin.thumbnail(self.test_tech_element_1),
        )

    @pytest.mark.django_db(False)
    def test_thumbnail_svg(self):
        self.assertIn(
            "<svg id=",
            self.techelement_admin.thumbnail(self.test_tech_element_2),
        )

    @pytest.mark.django_db(False)
    def test_thumbnail_no_media(self):
        self.assertEqual(
            "No Preview", self.techelement_admin.thumbnail(self.test_tech_element_3)
        )


@pytest.mark.django_db(False)
class TestEntranceURLAdmin(TestCase):
    def setUp(self):
        self.entranceurl = EntranceURL.objects.create(
            linked_to="test_element_1",
            greeting="test_greeting",
            url=generate_unique_url(),
        )
        self.entranceurl_admin = EntranceURLAdmin(EntranceURL, None)

    # While there is no good solution for unsupported parametrization in subclasses, use default values
    # https://docs.pytest.org/en/latest/how-to/unittest.html#pytest-features-in-unittest-testcase-subclasses
    # Github Issue: https://github.com/pytest-dev/pytest/issues/3484
    def test_has_view_permission(self, user=MockSuperUser(), expected=True):
        request = MockRequest()
        request.user = user
        self.assertIs(
            self.entranceurl_admin.has_view_permission(request=request), expected
        )


@pytest.mark.django_db(False)
class TestSessionsAdmin(TestCase):
    def setUp(self):
        Session.objects.create(
            session_key="test_session_key",
            session_data="test_session_data",
            expire_date=timezone.localtime(timezone.now()),
        )
        self.session_admin = SessionsAdmin(Session, None)

    # While there is no good solution for unsupported parametrization in subclasses, use default values
    # https://docs.pytest.org/en/latest/how-to/unittest.html#pytest-features-in-unittest-testcase-subclasses
    # Github Issue: https://github.com/pytest-dev/pytest/issues/3484
    def test_has_view_permission(self, user=MockSuperUser(), expected=True):
        request = MockRequest()
        request.user = user
        self.assertIs(self.session_admin.has_view_permission(request=request), expected)


@pytest.mark.django_db(False)
class TestPageViewAdmin(TestCase):
    def setUp(self):
        PageView.objects.create(
            url="test_url",
            user_agent="test_user_agent",
            ip="192.168.0.1",
            referer="test_referer",
            session=Session.objects.create(
                session_key="test_session_key",
                session_data="test_session_data",
                expire_date=timezone.localtime(timezone.now()),
            ),
            user=User.objects.create_user(
                username="test_user",
                email="",
                password="test_password",
            ),
        )
        self.pageview_admin = PageViewAdmin(PageView, None)

    # While there is no good solution for unsupported parametrization in subclasses, use default values
    # https://docs.pytest.org/en/latest/how-to/unittest.html#pytest-features-in-unittest-testcase-subclasses
    # Github Issue: https://github.com/pytest-dev/pytest/issues/3484
    def test_has_view_permission(self, user=MockSuperUser(), expected=True):
        request = MockRequest()
        request.user = user
        self.assertIs(
            self.pageview_admin.has_view_permission(request=request), expected
        )
