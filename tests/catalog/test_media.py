import random
import pytest

from django.utils import timezone

from catalog.models import Project, TechElement
from catalog.templatetags.media import (
    get_dalle_img,
    get_project_view_level,
    get_ts_len,
    get_blur,
)


def test_get_sample_img():
    assert get_dalle_img(random.randint(1, 14)) is not None


# Test all functions that require a setup such as
# 'get_view_level', 'get_ts_len' and 'get_blur'
class TestMediaFunctions:
    def setup(self):
        # First the tech stack, to populate the Projects' techstack field
        self.tech_element1 = TechElement(name="test_tech_element_1", icon_src="+")
        self.tech_element2 = TechElement(name="test_tech_element_2", svg="+")
        self.tech_element3 = TechElement(name="test_tech_element_3")

        self.project1 = Project(
            name="test_project_1",
            active=False,
            view_level=1,
            created=timezone.now(),
        )

        self.project2 = Project(
            name="test_project_2",
            active=False,
            view_level=2,
            created=timezone.now(),
        )
        self.project3 = Project(
            name="test_project_3", active=False, created=timezone.now(), view_level=3
        )

        self.project1.save()
        self.project2.save()
        self.project3.save()
        self.tech_element1.save()
        self.tech_element2.save()
        self.tech_element3.save()

        self.project1.techstack.add(self.tech_element1)
        self.project1.techstack.add(self.tech_element2)
        self.project2.techstack.add(self.tech_element3)

        self.project1.save()
        self.project2.save()

    @pytest.mark.django_db(False)
    def test_get_view_level(self):
        assert get_project_view_level(self.project1) == 1
        assert get_project_view_level(self.project2) == 2
        assert get_project_view_level(self.project3) == 3
        assert get_project_view_level() == 0

    @pytest.mark.django_db(False)
    def test_get_ts_len(self):
        assert get_ts_len(self.project1) == 2
        assert get_ts_len(self.project2) == 1
        assert get_ts_len(self.project3) == 0
        assert get_ts_len() == 0

    @pytest.mark.parametrize(
        "user_view_level, expected",
        [
            (0, (True, True, True)),
            (1, (False, True, True)),
            (2, (False, False, True)),
            (3, (False, False, False)),
        ],
    )
    @pytest.mark.django_db(False)
    def test_get_blur(self, user_view_level, expected):
        assert (
            get_blur(project=self.project1, user_view_level=user_view_level)
            is expected[0]
        )
        assert (
            get_blur(project=self.project2, user_view_level=user_view_level)
            is expected[1]
        )
        assert (
            get_blur(project=self.project3, user_view_level=user_view_level)
            is expected[2]
        )
