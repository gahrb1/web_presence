import pytest
from django.contrib.sessions.models import Session
from django.test import TestCase
from django.utils import timezone

from catalog.models import EntranceURL
from catalog.templatetags.media import generate_unique_url


class EntranceURLTestCase(TestCase):
    def setUp(self):
        test_session = Session.objects.create(
            session_key="test_session_key",
            session_data="test_session_data",
            expire_date=timezone.localtime(timezone.now()),
        )
        entrance_url = EntranceURL.objects.create(
            linked_to="test_element_1",
            greeting="test_greeting",
            url=generate_unique_url(),
        )
        EntranceURL.objects.create(
            linked_to="test_element_2",
            greeting="test_greeting",
            url=generate_unique_url(),
        )
        entrance_url.accessed_by.add(test_session)

    def test_creation(self):
        self.assertEqual(EntranceURL.objects.count(), 2)

    def test_uniqueness(self):
        self.assertNotEqual(
            first=EntranceURL.objects.get(linked_to="test_element_1"),
            second=EntranceURL.objects.get(linked_to="test_element_2"),
        )

    def test_accessed_by(self):
        self.assertEqual(
            EntranceURL.objects.get(linked_to="test_element_1").accessed_by.count(), 1
        )
        self.assertEqual(
            EntranceURL.objects.get(linked_to="test_element_2").accessed_by.count(), 0
        )


@pytest.mark.parametrize(
    "length,expected",
    [
        ((2, 3), False),
        ((3, 3), True),
    ],
)
def test_len_generate_unique_url(length: list[int], expected: bool):
    url_1 = generate_unique_url("test_host", length=length[0])
    url_2 = generate_unique_url("test_host", length=length[1])
    assert (len(url_1) == len(url_2)) == expected


def test_generate_unique_url():
    url_1 = generate_unique_url("test_host")
    url_2 = generate_unique_url("test_host")
    assert url_1 != url_2
