import pytest
from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase, RequestFactory

from catalog.views import index


class MiddlewareTests(TestCase):
    def setUp(self) -> None:
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="test_user",
            email="",
            password="test_password",
        )
        request = self.factory.get("/")
        request.user = AnonymousUser()
        self.response_default = index(request)

        request = self.factory.get("/")
        request.user = self.user
        self.response_authenticated = index(request)

    @pytest.mark.django_db(False)
    def test_e2e_middleware(self):
        self.assertLess(self.response_default.status_code, 400)

    @pytest.mark.django_db(False)
    def test_e2e_middleware_authenticated(self):
        self.assertLess(self.response_authenticated.status_code, 400)

    # def test_db_entrance_middleware(self):
    #     self.assertEqual(PageView.objects.all().count(), 2)

    # def test_db_entrance_middleware_authenticated(self):
    #     self.assertEqual(PageView.objects.filter(user_agent__isnull=False).count(), 1)
