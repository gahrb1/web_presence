from django.test import TestCase

from catalog.models import TechElement


class TechElementTestCase(TestCase):
    def setUp(self):
        TechElement.objects.create(name="test_element")

    def test___str__(self):
        self.assertEqual(
            "test_element -- "
            + str(TechElement.objects.get(name="test_element").id)[0:5],
            TechElement.objects.get(name="test_element").__str__(),
        )
